﻿using DemoOwin.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DemoOwin
{
    public class AuthRepository : IDisposable
    {
        //Comment added
        private AuthContext _ctx;

        private UserManager<IdentityUser> _userManager;

        public AuthRepository()
        {
            _ctx = new AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.UserName
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);
            return user;
        }

        public async Task<List<EMP>> GetAllUserDetails()
        {
            List<EMP> employeeList = new List<Models.EMP>();
            employeeList = _ctx.EMPs.ToList();
            return employeeList; 
        } 

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }
}