﻿using DemoOwin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DemoOwin.Controllers
{
    [RoutePrefix("api/Employee")]
    public class EmployeeController : ApiController
    {
        private AuthRepository _repo;
        public EmployeeController()
        {
            _repo = new AuthRepository();
        }

        [Route("Details")]
        public async Task<IHttpActionResult> Get()
        {
            List<EMP> result = await _repo.GetAllUserDetails();

            if (result != null)
                return Ok(result.Select(x=> new { x.EmployeeName,x.Salary} ));
            else
                return BadRequest();
        }
        
    }
}
