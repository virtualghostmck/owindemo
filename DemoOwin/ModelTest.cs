namespace DemoOwin
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelTest : DbContext
    {
        public ModelTest()
            : base("name=EmployeeAuth")
        {
        }

        public virtual DbSet<EMP> EMPs { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EMP>()
                .Property(e => e.EmployeeName)
                .IsUnicode(false);
        }
    }
}
