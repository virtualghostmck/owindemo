namespace DemoOwin.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMP")]
    public partial class EMP
    {
        [Key]
        public int EmployeeID { get; set; }

        [StringLength(50)]
        public string EmployeeName { get; set; }

        public int? Salary { get; set; }
    }
}
